# Introduction

![React](img/React.png){: style="height: 100px; float: left;"}
![Quart](img/Quart.png){: style="height: 100px; float: right;"}

This is a cookbook with multiple recipes (although no overall
narative) that are useful to build an async await stack. That is a
single page app, SPA, with a corresponding API microservice to serve
it. The SPA will utilise the [React](https://reactjs.org/) framework
and the microservice the [Quart](https://pgjones.gitlab.io/quart/)
framework. Both will extensively utilise the `async`/`await` keywords
recently added to Javascript and Python.

A working example of many of these recipes is my personal website,
[pgjones.dev](https://pgjones.dev), with the source available on
[gitlab](https://gitlab.com/pgjones/pgjones_dev/).
