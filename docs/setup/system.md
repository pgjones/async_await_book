# System requirements

Node and Python are required to build and run the code described in
this book. Specifically the versions Node > 10 and Python > 3.7 must
be used.

The installation instructions for Node and Python will depend on the
system you are using. It is therefore best to search for instructions
for your exact system. Instructions for linux based systems (including
Mac and the Windows Subsystem for Linux) are given below.

## Using brew

![Brew](img/brew.png){: style="height: 80px;float: left"}
![Linuxbrew](img/linuxbrew.png){: style="height:80px; float: right"}

Homebrew is a fantasic package manager that whilst originally written
for MacOS now has a Linux version. If you are working on a Mac you
will need to follow the instructions at [brew.sh](https://brew.sh/)
and if you are working on a Linux system you will need to follow the
instructions at [linuxbrew.sh](http://linuxbrew.sh/) to install brew.

Once you've installed brew you can use brew to install Node and
Python,

    brew install node
    brew install python

by default you will get the latest versions, which will be at least
Node 10 and Python 3.7.
