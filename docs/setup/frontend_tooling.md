# Setting up the frontend tooling

![Yarn](img/yarn.png){: style="height:100px; float: right;"}

The frontend will use [yarn](https://yarnpkg.com) to manage the
javascript packages and to act as the tooling entry point. In this
regard `yarn` fullfils the same role as `poetry` does for the backend
code.

## Installing Yarn

It is up to you how and where you install yarn, however the easiest
way is to follow the
[instructions](https://yarnpkg.com/en/docs/install).

## Why Yarn

Node comes with a package manager called npm, which has roughly the
same feature set as Yarn. This wasn't always the case and until npm
version 5 Yarn had superior features, notably a lock file. In this
author's opinion Yarn is a much more robust and better engineered
package manager whereas npm represents a risk.
