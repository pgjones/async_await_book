# Setting up the backend tooling

![Poetry](img/poetry.png){: style="height: 60px; float: right;"}

The backend will use [poetry](https://poetry.eustace.io/) to manage
the python packages and to act as the tooling entry point. In this
regard `poetry` fullfils the same role as `yarn` does for the frontend
code.

## Installing Poetry

It is up to you how and where you install poetry, however the easiest
way is to follow the
[instructions](https://poetry.eustace.io/docs/#installation).

## Why Poetry

Python has some very mature package management tooling, notably pip,
virtualenv, and pip-tools whereas Poetry is quite new and certainly
not yet mature. In addition there are immature tools such as pipenv
and hatch. Poetry is the recommendation over all of these, and should
be the only tool you need.

What sets Poetry apart is its direct support of
[PEP-518](https://www.python.org/dev/peps/pep-0518/)
i.e. pyproject.toml support and the usage of a lock file. The lock
file means that it is possible to know that the exact same depdencies
are in use wherever Poetry and the lock file are used, which prevents
a class of bugs that cannot be reproduced locally. Poetry itself
explains its motivations in its
[README](https://github.com/sdispater/poetry#why).
