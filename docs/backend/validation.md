# Validating request and response JSON

JSON is a very well supported and easier format to use as a
communication standard between the frontend and backend. Whilst both
the frontend and backend will validate that any data sent/received is
valid JSON it cannot validate that the data itself is as
expected. This is especially problematic with data received by the
backend as any client could send anything to the backend - potentially
in an attempt to cause server errors.

I typically add validation to the backend to ensure that the received
data is valid and as expected and to ensure that I don't reply with
data that would cause an error to the frontend. This is a form of
[design by contract](https://en.wikipedia.org/wiki/Design_by_contract).

## Validating JSON

There is a well established standard for defining the structure of
JSON data called [JSON schema](https://json-schema.org/). It works by
defining the structure as a schema written itself in JSON, for example
a schema that defines a simple string in JSON is,

```json
{ "type": "string" }
```

In python there are a few libraries that can take a JSON schema and
validate that JSON data conforms to it. The most popular library is
[jsonschema](https://github.com/Julian/jsonschema) however I prefer
[Fast JSON Schema](https://github.com/horejsek/python-fastjsonschema)
due to its performance.

## Validating request JSON

The API I like to use is to decorate a route with the schema that
applies to it, for example,

```python
@app.route("/", methods=["POST"])
@validate_request(
    {
        "type": "object",
        "properties": {"count": {"type": "integer"}},
        "required": ["count"],
        "additionalProperties": False,
    }
)
async def set_count():
    data = await request.get_json()
    count = data["count"]
    ...
```

will reply with a 400 bad request response if a request is received
without JSON data, or if the data does not look similar to `{"count":
1}`. Specifically the data must be an object, with a single count
property that has a integer value.

## Validating response JSON

The API I like to use is to decorate a route with the schema that
applies to it for each given status code, for example,

```python
@app.route("/")
@validate_response({
    200: {
        "type": "object",
        "properties": {"count": {"type": "integer"}},
        "required": ["count"],
        "additionalProperties": False,
    }
})
async def get_count():
    return {"count": 2}, 200
```

will raise an exception that will be logged on the server. This
prevents any broken data reaching the client (backend returns a 500
response) and allows the programming error to be caught, logged and
then hopefully fixed.

## Implementation

To implement these functions the following will work (you are likely
to want to adjust it to your own particular needs),

``` python
import fastjsonschema
from quart import request
from quart.exceptions import BadRequest


def validate_request(schema: Dict[str, Any]) -> Callable:
    """This validates the request JSON.

    If there is no JSON in the body, or the JSON doesn't validate this
    will trigger a 400 response.
    """
    validator = fastjsonschema.compile(schema)

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        async def wrapper(*args: Any, **kwargs: Any) -> Any:
            data = await request.get_json()
            try:
                validator(data)
            except fastjsonschema.JsonSchemaException:
                raise BadRequest()
            else:
                return await func(*args, **kwargs)

        return wrapper

    return decorator


def validate_response(schemas: Dict[int, Dict[str, Any]]) -> Callable:
    """This validates the response JSON.

    The schemas are keyed by status code.
    """
    validators = {
        status: fastjsonschema.compile(schema)
        for status, schema in schemas.items()
    }

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        async def wrapper(*args: Any, **kwargs: Any) -> Any:
            result = await func(*args, **kwargs)
            response = await make_response(result)
            if response.status_code in validators:
                validators[response.status_code](await response.get_json())

            return response

        return wrapper

    return decorator
```
