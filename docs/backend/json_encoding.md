# Encoding custom objects into JSON

Using JSON to communicate between the frontend and backend requires
that all the objects in Python sent are encoded to JSON. This is often
an issue with `datetime` objects, as the default encoder doesn't
support datetimes. The simple solution is to write a custom encoder
and inform the Quart app to use it,

```python
from datetime import date, datetime
from json import JSONEncoder

class CustomEncoder(JSONEncoder):
    def default(self, obj: object) -> str:
        if isinstance(obj, datetime):
            return obj.isoformat(timespec="seconds")
        elif isinstance(obj, date):
            return obj.isoformat()
        else:
            return super().default(obj)

app.json_encoder = CustomEncoder
```

ISO formatted dates and datetimes offer the best support client side
and are a very clear format. The timespec will depend on your usage,
my usage typically requires only second resolutions.

## Further example, encoding Decimals

To avoid floating point errors and issues when working with values
that represent money I use the `Decimal` class in the stdlib. This is
another object that needs a custom encoder to encode to JSON,

```python
from decomal import Decimal
from json import JSONEncoder

class CustomEncoder(JSONEncoder):
    def default(self, obj: object) -> str:
        if isinstance(obj, Decimal):
            return str(obj)
        else:
            return super().default(obj)
```

I encode it as a string, rather than a number as I wish to avoid
precision and accuracy issues during the encoding. Client side I use
the [decimal.js](https://github.com/MikeMcl/decimal.js/) for the same
reason.
