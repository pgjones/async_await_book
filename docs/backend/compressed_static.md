# Serving compressed static files

Quart will by default serve static files, however it will serve these
files compressed. Instead it will always serve the uncompressed
version. Most clients however will accept compressed files (as can be
seen by inspecting the request accept-encodings).

To serve gzip'd compressed files you should first gzip the files as
part of the build step (this is more efficient that compressing each
file on request). For example,

```shell
gzip --keep static/js/*
```

note the `--keep` is important to ensure that the uncompressed files
are present (not all clients accept compressed files).

Now you need to override the Quart static serving code to send a
compressed file if it is present and if the client accepts it, this
snippet below does just that,

```python
import mimetypes

from quart import Quart, request, Response
from quart.exceptions import NotFound
from quart.static import safe_join, send_file


class CustomQuart(Quart):

    async def send_static_file(self, filename: str) -> Response:
        path = safe_join(self.static_folder, filename)
        gzip_path = path.with_suffix(path.suffix + ".gz")
        if "gzip" in request.accept_encodings and gzip_path.is_file():
            response = await send_file(gzip_path)
            response.content_type = mimetypes.guess_type(path.name)[0]
            response.content_encoding = "gzip"
            return response
        elif path.is_file():
            return await send_file(path)
        else:
            raise NotFound()

app = CustomQuart(__name__)
```
